### What is this repository for? ###

* A Developer framework to make addon development, plane creation, virtual cockpits and more by using Lua, etc.. easily accessible to the masses..
* Version: 0.0.1



### How do I get set up? ###

* Summary of set up
 Currently the replacement files will drop into FlyWithLua - simply extract directly in that folder - you'll notice the same folder structure - and it will add nicer function libraries, helper functions, objects and more! No default files will be overwritten therefore when updating you can freely overwrite files from this repo.. All files are uniquely named in this repo so as to not interfere with FlyWithLua default installation...

 A primary reason to use this system is because it acts like a wrapper within FlyWithLua... This allows addon developers to create addons for X-Plane 11 in a uniform manner, and if a script breaks due to an update, it will only need to be fixed within our system in most cases. Additionally we can retain backwards compatibility, add database support for saving / loading data for multiplayer, and more.


* Configuration
 There is nothing to configure at this moment.


* Dependencies
  FlyWithLua which can be downloaded at http://forums.x-plane.org/index.php?/files/file/38445-flywithlua-complete-edition-for-x-plane-11-windows-linux-mac-os-x-version/ or http://forums.x-plane.org/index.php?/files/file/35579-flywithlua-core-edition-for-x-plane-11-windows-linux-mac-os-x-version/
  and you can view their forums at http://forums.x-plane.org/index.php?/forums/forum/188-flywithlua/



### Contribution guidelines ###

* Coding standards follow a strict regiment:
 Variables without underscores are globals.
 Variables starting with 1 underscore ( _ ) denotes a local variable either from a function argument, or local _xxx = xyz; in a file - the scope can be local to the file, function, etc..
 Variables starting with 2 underscores ( __ ) denotes an internal variable which shouldn't be modified by anything outside of the object which controls it - the scope should be within an object or library which manages it.
 Variables in all UPPERCASE are CONSTants or ENUMeration - the scope can be local to a file, or global - if a CONSTant or ENUMeration is local to a file, it does not require an underscore.
 All brackets, parenthesis, commas, etc.. such as empty tables, table references, function calls and declarations, etc.. will have one space gap - airy code so if ( blah == blahy ) then ... end or function xyz( a, b, c, d ) ... end and local blah = { }; blah[ 123 ] = "xyz"; ...
 All multi-line tables will end the row with a semi-colon ( ; ).
 All Single-Line tables will use commas.
 All function declarations will have no fewer than 3 comment-lines above with the center containing a description and exactly 2 blank lines between anything above the first comment-line or below the function end.
 All files will have no fewer than 3 comment-lines at the top of the file with the center containing a description and exactly 2 blank lines below the last comment-line. The author of the file or the majority of the file will be listed directly after the description using the format // <Description> - <Author> excluding the brackets...
 All functions co-authored or authored by a party OTHER than the file author is REQUIRED to give an author credit after the description of the function on the center description comment-line OR be granted an Author line using the format: // @Author: <Author> <Website> <Contact> <LICENSE> with website and contact being optional according to the author.. and at the end of the file the following code must exist:


//
//
//
hook.Add( "SetupCredits", "RegisterCredits::Path/To/Filename.lua", function( )
  // Create a new credit in the registry..
  local _credit = credits:New( );

  // Add the author ID such as a nickname, their name, website and contact - if this is frequently repeated in multiple files then an author file can be established to register their name, etc... then you only need to use _credit:AddAuthor( "ID" ); instead of the full info each time.. It is required that each file with different work contain this because if a file is removed then the automated credits system will be automatically updated..

  -- All AddWork lines added after this line will be associated with this author.. Alternatively you can use SetAuthor( ID );
  _credit:AddAuthor( "ID", "Name", "Website", "Contact" );

  -- _credit:AddWork( <Table/Function/String> _data ); - so it can be a function, or text entry, or a table containing an object or library
  _credit:AddWork( draw.Line ); -- as an example..
  _credit:AddWork( draw.Rectangle ); -- can keep adding work...

  -- This isn't required
  _credit:Publish( );

  -- Note: The LAST AddAuthor call you make gets all of the NEXT AddWork lines associated with that author... So don't add 10 authors then add all work... Alternatively I can provide an AddWorkToAuthor function which can allow you to specify the author id, but having a stack-based system for adding work is neater...
end );



More to come...





* Coding standards which will be enforced after CLua has been implemented:
 C Style operators will be used in ALL code - This will be active as soon as I compile a custom version of Lua into the game.



### Who do I talk to? ###
 Josh 'Acecool' Moser - Acecool™ is a valid Trademark of Acecool / Acecool Company and its subsidiaries - Just as all of my work is protected under copyright law - Trademarks and Copyright are automatic on new brands which have never been used before and new work upon creation. The Acecool brand was created in 1989 with websites being registered shortly after - the web-domains were stolen from me by GoDaddy after my domain management service went out of business and they sold my domains, without authorization, to another company and then GoDaddy and sold my domains to a Trademark infringer and they have been successfully deactivated recently. I am in the process of reacquiring my domain names.


### Full Description ###

 This will either be an extension to FlyWithLua or a replacement. I plan on incorporating Lua into every aspect of X-Plane along with adding hooks ( Broadcasting calls ) and more to every important and other aspect of the game to allow modders to come in and quickly modify behavior or create total replacements, etc...
 I plan on supporting GLua / CLua - which is Vanilla Lua with C-Style operators ( !, &&, ||, continue [ although this is in so it is no longer required to add the logic which is a simple inverted if statement ], etc.. )

 Some ideas: http://forums.x-plane.org/index.php?/forums/topic/125269-redesigning-flywithlua/

 ### Thread with ideas ###
 So lets say you want to create a panel to monitor engines you could do:

```lua
//
// Called when after the plane is loaded while panels are loaded
//
hook.Add( "OnSetupPanels", "SetupEngineMonitor", function( )
	local _eng_panel = vgui.Create( "EnginePanel" );
	_eng_panel:AddEngine( GetLocalPlane( ):GetEngine( 1 ) );
end );
```

and be done.
Or say you want to monitor an engine to give it a random failure:

```lua
//
// Tick would be for each time physics is called or we could use Think for that, and one would be for each frame so if something is done more than once per frame there'd be a hook for that and one for the other...
//
hook.Add( "Tick", "EngineFailureManagement_this is just a name", function( _plane )
    // Simple helper so we don't need to re-reference each time
    local _eng1 = _plane:GetEngine( 1 );

   	// If engine 1 rpm is over 900
    // and engine 1 temp is over 200 ( celcius or something could be set as default or we could use a temperature object so GetTemperature( ):GetCelcius( ) )
    // and in rare circumstances ( math.rand would return between 0 and 1 float )
    // and engine 1 isn't on fire
    // and engine 1 hasn't caught fire previously this flight
    // and our plane has been in the air for more than 10 minutes
    if ( _eng1:GetRPM( ) > 900 && _eng1:GetTemperature( ) > 200 && math.rand( ) > 0.9999 && !_eng1:HasFailure( FAILURE_FIRE )&& !_eng1:HadFailure( FAILURE_FIRE ) && _plane:GetFlightTime( ) > TIME_MINUTE * 10 ) then
      // Set engine 1 on fire... - simulate bird strike maybe?
      _eng1:AddFailure( FAILURE_FIRE );

      // Bird strike maybe? - so have a random control surface fail which is close to engine 1 - could use the object or an enumeration...
      _plane:AddFailureNearRegion( FAILURE_RANDOM_CONTROL_SURFACE, _eng1 || ENGINE_1 );
    end
end );
```


So if engine 1 rpm is over 900 and temperature over 200 and in very rare circumstances and it isn't currently on fire and it hasn't experienced a fire yet and the flight time is over 10 minutes then set it on fire... and damage a random control surface near engine 1... or call util.CreateExplosion( data ) and have that cause damage...
 
Alternatively, if you want to allow a window for it to fail instead of just over ( and under using the FlightTime function ) and so the hook doesn't need to execute before or after needed:

```lua
//
// Add an argument which can be a time, or a function to act as a delay, or if the hook executes...
//
hook.Add( "Tick", "NameofIt", function( _plane )

end, function( _plane )
	local _ft = _plane:GetFlightTime( );

    // Remove the hook after x time... This could be added to the hook itself, but here we could remove outside of standard code...
    if ( _ft >= 900 ) then
    	hook.Remove( "Tick", "NameOfit" );
    end

  	// If true then allow the hook to process - alternatively: math.InRange( _ft, 600, 900 );
   	return ( _ft > 600 && _ft < 900 );
end );
```
 
Additionally hooks such as SetupVirtualCockpit could exist and, as I suspect the default game does, you can create panels such as altimeter, etc... and then simply position them on the surface so Lua could be used so plane modelers don't need to recreate each instrument over and over... A lot of instruments are identical between aircraft, sometimes the casing is different, etc.. if the game doesn't already re-use elements then Lua could be used so people that make planes can set up an amazing virtual cockpit in minutes... Toggle switches and everything could be basic objects, then you can extend to create taxi light button or simply SetSwitchType( SWITCH_TAXI_LIGHTS ) - all of the switches could use a similar system to my vehicle attachment system so you can be in game and set up the exterior and internal of the vehicle then save it and it gets propagated to the server and to clients ( although this could be without net-code side of things ) and then it reflects the updates - then plane developers don't need to write the Lua aspect of it, simply generate a text file..
 
If interested, I'd be interested in joining the project to help add in the new structure, hooks, etc... What I'm proposing will allow more modders to come forward and easily make their ideas reality. As CLua / GLua syntax is backwards compatible with Vanilla Lua, it wouldn't matter if the code used && or and... etc.. Note: I use underscores in my code standard to denote local variables, none for global, all caps for enumeration / constant, 2 underscores for internal values that shouldn't be played with such as object data tables ( which would have accessors to modify the data properly ) etc... I also use semi-colons even though they're treated as white-space except in tables - I use semicolons in tables for multiline tables, commas for single-line.
 
I haven't looked at the entire modding scene yet to see everything but I am not a fan of how the current functions are set up... I believe they could be added to libraries, etc.. to make the code read much better and instead of using RunString for all of the code, they could be extended to allow functions, objects, etc...
 
If not interested, I may go the route of making my own addon but with my neck and back issues - it takes me a very long time to do things which is why I'm suggesting it here because it'd be easier to adapt an existing, established, mod than to recreate it and then go from there.