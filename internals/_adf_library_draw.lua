--//
--// Draw & Surface Library ( So instead of surface.SetDrawColor it'll be draw.SetColor and instead of using 3 or 4 args, it'll use a Color Object and it'll auto switch between draw color 3f / 4f depending on what is necessary )
--//
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- >8 -- -- -- -- --
-- FlyWithLua graphics library V1.2
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- we can use these functions, to access to OpenGL:
--
-- glBegin_POINTS( );
-- glBegin_LINES( );
-- glBegin_LINE_STRIP( );
-- glBegin_LINE_LOOP( );
-- glBegin_POLYGON( );
-- glBegin_TRIANGLES( );
-- glBegin_TRIANGLE_STRIP( );
-- glBegin_TRIANGLE_FAN( );
-- glBegin_QUADS( );
-- glBegin_QUAD_STRIP( );
-- glEnd( );
-- glVertex2f( _x, _y );
-- glVertex3f( _x, _y, _z );
-- glLineWidth( _w );
-- glColor3f( _r, _g, _b );
-- glColor4f( _r, _g, _b, _a );
-- glRectf( _x, _y, _x2, _y2 );
--
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- If you are not familiar with OpenGL, we create some nice functions to be used in 2D window mode:
-- -- -- 8< -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
draw = draw or { };


--//
--//
--//
--// local DRAW_DEFAULT_VALUE = 0;



--//
--//
--//
function draw.SetColor( _color )
	--// If the color is nil or not set then don't change anything...
	if ( not _color or not _color:Exists( ) ) then return; end

	--// If our color object has alpha set, then we'll use glColor4f which has an alpha arg
	if ( _color:HasAlpha( ) ) then
		return glColor4f( _color.r, _color.g, _color.b, _color.a );
	end

	--// Otherwise we'll use 3f which doesn't have alpha set - NOTE: On either of these we can simply use unpack( _color )
	return glColor3f( _color.r, _color.g, _color.b );
end


--//
--// Draw Simple-Line - Instead of requiring both positions, it takes 1 position and then you tell it the offset of the second..
--//
function draw.SimpleLine( _x, _y, _offset_x, _offset_y )
	return draw.Line( _x, _y, _x + _offset_x, _y + _offset_y );
end


--//
--// Draw Line from x and y position..
--//
function draw.Line( _x, _y, _x2, _y2
	--// For now, we'll include what the default function was in order to alter all scripts later..
	-- return draw_line( _x, _y, _x2, _y2 );

	--// This is FlyWithLua almost exactly default code - simply shortened...
	glBegin_LINES( );
		glVertex2f( _x or 0, _y or 0 );
		glVertex2f( _x2 or 0, _y2 or 0 );
	glEnd( );
end


--//
--//
--//
function draw.SimpleRectangle( _x, _y, _w, _h )
	return draw.Rectangle( _x, _y, _x + _w, _y + _h );
end


--//
--//
--//
function draw.Rectangle( _x, _y, _x2, _y2 )

end