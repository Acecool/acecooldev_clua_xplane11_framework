--//
--// Color Object - As this is an object, we'll use : so that self can be used when an object is created.. If color:Get( ) is used without being tied to an object, we'll return a default color - Josh 'Acecool' Moser
--// Note: It is up to the user to purge colors no longer used from memory if a GC isn't enabled in X-Plane FlyWithLua system - I could set up a table, code will be below but commented out, which keeps track of all
--//		of the colors, but by keeping a reference, they'll pile on especially if the developer doesn't create them outside of draw hooks, etc...
--//
--// Note: All of the comments are for those that are learning the language - these may be used for educational use and in classrooms - they may not be used for commercial use without prior authorization.
--//


--//
--// INIT
--//
color = color or { };


--// Simple map which allows other keys to be used for the __data table... so if _color.green is used it'll be translated as _color.__data.g
color.__data_map = {
	red		= "r";
	green	= "g";
	blue	= "b";
	alpha	= "a";
};


--//
--// Set up the index function so when r / red, g / green, b / blue, a / alpha are indexed, it redirects them to the data table.. I could set the defaults here too...
--//
function color:__index( _key )
	// Helper so I don't need to use self.__data and so I can check if inside of an instance...
	local _exists, _data = self:Exists( );

	--// __data exists then we're in an instance of color - check to see if our key or mapped key exists inside of __data then we'll use that ( so r, g, b, a will redirect there but nothing else )
	if ( _exists ) then
		--// Create some helpers so we're not duplicating code

		--// Lowercase _key
		local _k = string.lower( _key );

		--// Reference the self.__data table directly to see if the fully lowercase string referenced by _key exists as a valid entry. Alternatively, check for data using the data map...
		local __data = _data[ _k ] or _data[ self.__data_map[ _k ] ];

		--// Now, if there is actual data stored in the remapped data reference or in the unmodified key ( other than string.lower ) then return, otherwise fall out of this if and go to the default..
		if ( __data ) then
			return __data;
		end
	end

	--// For our default return, we'll return the data at "color"[ _key ] - if all we did was use this line then we wouldn't need this function - we could simply do color.__index = color;
	--// but because we added a shortcut for r, g, b, a we need to redirect those to the appropriate table...
	return rawget( self, _key );
end


--//
--// Set up the __tostring method which is for debugging purposes as it correlates to the print function - this should NEVER be used for data serialization... For data serialization create a different function...
--//
function color:__tostring( )
	// Helper so I don't need to use self.__data and so I can check if inside of an instance...
	local _exists, _data = self:Exists( );

	--// If we are inside of a color object / instance
	if ( _exists ) then
		--// Return the data - I modify the print function to support language strings - text prefixed with #....
		--// This would basically be Color_Object_toString is: Color( %s, %s, %s, %s ) - r, g, b, a will be 0-255 as a number ( converted to a string ) OR will so it could be output as the following:
		--// Color( 255, 0, 0, Alpha Undefined ); or Color( 255, 255, 255, 255 ); or Color( Red Undefined, Green Undefined, Blue Undefined, Alpha Undefined );, etc..
		--// Note: As there is only meant to be 1 return, the language function must be called here - but as it hasn't been coded yet I'll leave them as is for later...
		-- return "#color_object_tostring", self.__data.r or "#color_object_red_undefined", self.__data.g or "#color_object_green_undefined", self.__data.b or "#color_object_blue_undefined", self.__data.a or "#color_object_alpha_undefined";


		--//
		--// Localized helper function which is used here and nowhere else... Even though _data from above is in the scope for this function, because it is defined as an argument it ignores the _data reference above and uses what is used as the argument..
		--//
		local function __tostring_helper( _data )
			return ( isnumber( _data ) ) and tostring( _data ) or "Undefined";
		end


		--// And, instead of giving no information, I'll return how it should look but with hard-coded language which I despise... But now it'll be: Color( Red: 0-255 or Undefined, Green: 0-255 or Undefined, Blue: 0-255 or Undefined, Alpha: 0-255 or Undefined )
		return "Color( Red: " .. __tostring_helper( _data.r ) .. ", Green: " .. __tostring_helper( _data.g )  .. ", Blue: " .. __tostring_helper( _data.b ) .. ", Alpha: " .. __tostring_helper( _data.a ) .. " );";
	end

	--// If we're not in an instance, then we can output: Color( R, G, B, A are all Undefined ); or whatever else we want...
	return "#color_object_tostring_error_not_in_instance";
end


--//
--// This is so that when you define local _color = color:New( 255, 0, 0, 255 ); that you don't need to use _color.r, g, b, a.... you can use _color( ) and have the data-points returned as a table and unpacked...
--//
function color:__call( ... )
	return self:Get( ... );
end


--// Set up the meta-table / object type for when I redirect Lua type( _data ); function to support custom types so that meta-tables / objects can be reported as such instead of "table" which helps identify what data-type they are..
color.__type = "color";


--//
--// Create a new color object and return the reference..
--//
function color:New( _r, _g, _b, _a )
	--// Create a new table to store our object properties
	local _object = {
		--// Create a new color.__data table - note: if this was defined above, then it would be a static data-scope. Because we define it in here, it is a private data-scope.
		__data = {
			--// If Red is defined set it - Here I am setting all to 255, except alpha, but I can keep them as nil and use the color:Get( ) to return a default color if not set... I also verify the data-type here..
			r = ( isnumber( _r ) ) and _r or 255;

			--// Green
			g = ( isnumber( _g ) ) and _g or 255;

			--// Blue
			b = ( isnumber( _b ) ) and _b or 255;

			--// Alpha
			a = ( isnumber( _a ) ) and _a or nil;
		};
	};

	--// Initiate the meta-table.. Normal table on the left which is _object that we wish to apply to a meta-table layout which is color but we can use self for now...
	setmetatable( _object, self );

	--// Now that _object has been infused with the functionality of color, all of the functions will work so it behaves as an object with its own data table... We'll return it so it can be used immediately...
	return _object;
end


--//
--// Helper to determine whether self as it is called is an instance or if it is the parent...
--//
function color:Exists( )
	--// I use rawget here so that this can be used in all functions... I also return the object as a second argument so it doesn't need to be done elsewhere when needed... so it'll return true/false, data or nil...
	local _data = rawget( self, "__data")

	--// Return true / false, data / nil
	return ( ( istable( _data ) ) and true or false ), _data;
end


--//
--// Unloads a color from memory...
--//
function color:UnLoad( )
	// Helper so I don't need to use self.__data and so I can check if inside of an instance...
	local _exists, _data = self:Exists( );

	--// If we're inside of an instance ( ie self.__data is table )
	if ( _exists ) then
		--// For every entry in __data...
		for _key, _value in pairs( _data ) do
			--// Set the reference to nil - since all of these are simple data-types this will clean them up from memory.. If they were advanced objects we'd need to go through them...
			_data[ _key ] = nil;
		end

		--// Then unload the __data table by setting to nil - I can't just use _data = nil because that'll unset _data, not the actual table...
		self.__data = nil;

		--// Clear our locals..
		_exists = nil;
		_data = nil;

		--// Finally, set itself to nil...
		self = nil;
	end
end


--//
--// Return the color - it could be used as simply local _color = color:New( 255, 0, 0, 255 ); for red and _color is then it...
--//
function color:Get( _unpacked )
	// Check if in existence and get the data..
	local _exists, _data = self:Exists( );

	// If we're in an instance
	if ( _exists ) then
		// If we want it in unpacked form ( may not work as draw.SetColor( _color( true ) ), may need to be: draw.SetColor( unpack( _color( ) ) ) ) but there'll be a helper build in...
		if ( _unpacked ) then
			return _data.r, _data.g, _data.b, _.data.a;
		end

		return { _data.r, _data.g, _data.b, _.data.a };
	end

	return nil;

	--// This won't work because ternaries can only return ONE argument... So if it is as a table, it's fine... otherwise, you'll only get 1 arg..
	-- return ( ( _exists ) and ( ( _unpacked ) and _data.r, _data.g, _data.b, _.data.a or { _data.r, _data.g, _data.b, _.data.a } ) or nil );
end


--//
--// Returns whether or not this color object has Alpha set...
--//
function color:HasAlpha( _unpacked )
	// Check if in existence and get the data..
	local _exists, _data = self:Exists( );

	// Return the result...
	return ( _exists and isnumber( _data.a) ) and true or false;
end